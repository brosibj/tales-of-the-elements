package brosious.talesoftheelements;

import java.util.ArrayList;

import brosious.util.AnsiColor;

public class GameLogger {
	private static final GameLogger INSTANCE = new GameLogger();
	public final int TYPE_CMBT = 0;
	public final int TYPE_LOOT = 1;
	public final int TYPE_PATK = 2;
	public final int TYPE_EATK = 3;
	public final int TYPE_PDOT = 4;
	public final int TYPE_EDOT = 5;
	public final int TYPE_ENV = 5;
	public final int TYPE_CHAR = 5;
	private int historySize = 150;
	private ArrayList<String> history;
	
	private final AnsiColor[] COLORS = {new AnsiColor(AnsiColor.BLACK),
			new AnsiColor(AnsiColor.YELLOW), 
			new AnsiColor(AnsiColor.BLUE),
			new AnsiColor(AnsiColor.RED), 
			new AnsiColor(AnsiColor.LIME), 
			new AnsiColor(AnsiColor.GREEN), 
			new AnsiColor(AnsiColor.BLUE), 
			new AnsiColor(AnsiColor.NAVY)};
	private final String[] TAG = {"CMBT","LOOT","PATK","EATK","PDOT","EDOT", "ENV", "CHAR"};
	
	public GameLogger() {}
	
	public void log(int type, String message){
		String parsed = "["+TAG[type]+"] "+message;
		COLORS[type].outln(parsed);
		history.add(parsed);
		if (history.size() > historySize){history.remove(0);}
	}
	
	//TODO: More specific logger functions for different uses.
	
	public String retLog(int type, String message){
		String parsed = "["+TAG[type]+"] "+message;
		history.add(parsed);
		if (history.size() > historySize){history.remove(0);}
		return COLORS[type].colorize(parsed);
	}

	public void combat       (String m) {log(this.TYPE_CMBT, m);}
	public void loot         (String m) {log(this.TYPE_LOOT, m);}
	public void playerAttack (String m) {log(this.TYPE_PATK, m);}
	public void enemyAttack  (String m) {log(this.TYPE_EATK, m);}
	public void playerDOT    (String m) {log(this.TYPE_PDOT, m);}
	public void enemyDOT     (String m) {log(this.TYPE_EDOT, m);}
	public void enviornment  (String m) {log(this.TYPE_ENV,  m);}
	public void character    (String m) {log(this.TYPE_CHAR, m);}
	
	public static GameLogger getInstance(){return INSTANCE;}
	
	public void setHistorySize(int s){this.historySize = s;}
	public int historySize(){return this.historySize;}
	public String getMessage(int i){return this.history.get(i);}
	public String[] getHistory(){return (String[]) this.history.toArray();}
	public String[] getLastMessages(int num){
		ArrayList<String> ret = new ArrayList<String>();
		
		if (this.history.size() > num){
			int index = this.history.size() - num - 1;
			for (int i=index; i<(index+num); i++) ret.add(this.history.get(i));
		}
		else ret = this.history;
		
		return (String[]) ret.toArray();
	}
}
