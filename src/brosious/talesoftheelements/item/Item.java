package brosious.talesoftheelements.item;
import java.util.ArrayList;

import brosious.talesoftheelements.NPC;
import brosious.util.HighQualityRandom;


public class Item {
	HighQualityRandom r;

	private int rarity;
	private int requiredLevel = 0;
	private int value = 0;
	private long seed;
	private NPC mob;
	private int aroundLevel;
	private boolean useRandomSeed;
	private Boolean useRandomRarity = null;

	public ArrayList<Integer> thisRarity = new ArrayList<Integer>();

	
	public Item(){
	}
	
	public void aroundLevel(int level){this.aroundLevel = level;}

	public void genRandomSeed(){
		r = new HighQualityRandom();
		this.seed = r.seed();
		this.useRandomSeed = true;
	}
	
	public void setSeed(long seed){
		r = new HighQualityRandom(seed);
		this.seed = r.seed();
		this.useRandomSeed = false;
	}
	
	public static long findSeedWithRarity(int rarity){
		HighQualityRandom r = new HighQualityRandom();
		while ((r.nextInt() % NPC.CHANCE_RARITY[0].length) != rarity){
			r = new HighQualityRandom();
		}
		return r.seed();
	}
	
	public void useRarity(int rarity){this.rarity = rarity; this.useRandomRarity = false;}
	public void setMobDrop(NPC mob){this.mob = mob; this.useRandomRarity = true;}
	public void useRandomRarity(){this.useRandomRarity = true;}
	public void create(){
		if (r == null) throw new RuntimeException("Seed not initialized.");
		else {
			if (!useRandomSeed){ //use specified seed;
			} else {
				if (useRandomRarity){
					if (mob != null){ //use random seed with mob rarity table 
					} else { //use random seed with custom rarity table
					}
				} else { //random seed used but specified rarity
					r = new HighQualityRandom(Item.findSeedWithRarity(rarity));
				}
			}
		}
		
		rarity = r.nextInt();
		if (aroundLevel > 3) requiredLevel = r.nextInt(aroundLevel - 2, aroundLevel + 3);
		else requiredLevel = r.nextInt(aroundLevel, aroundLevel + 2);
		value = (int) ((requiredLevel + r.nextInt(0,2) + (requiredLevel / 2)) * (requiredLevel * 2.25));
		
	}
	
	//TODO Store actual mob object instead of mob type.
	public NPC droppedFrom(){return this.mob;}
	public int rarity(){return this.rarity;}
	public int requiredLevel(){return this.requiredLevel;}
	public int value(){return this.value;}
	public int sellValue(){return (int) (this.value * 0.75);}
	public long seed(){return this.seed;}
	
}
