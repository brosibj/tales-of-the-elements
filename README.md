# Tales of The Elements #

An attempt at a simple roguelike with a somewhat interesting storyline, engaging gameplay, and portability. Uses LWJGL, mini2Dx, and my own util library.

## Features ##

### Implemented ###
* Nothing. This game doesn't exist yet, the base code is too small.

### Planned ###

* 4 Unique classes: **Knight**, **Wizard**, **Rogue**, and **Archer**
* Elemental influence becoming more and more prominent throughout the game.
* Android support
* Full implementation of mini2Dx for tile based graphics.

## Compiling ##
If you wish to compile this yourself, it is dependent on the following libraries:

* [LWJGL](http://www.lwjgl.org)
* [mini2Dx](http://www.mini2dx.org)
* My [util](https://bitbucket.org/Fearlessagent/util) library