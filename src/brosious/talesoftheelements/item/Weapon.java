package brosious.talesoftheelements.item;

import brosious.talesoftheelements.Player;

public class Weapon extends Item {
	private int type;

	public final static int TYPE_WAND = 0;
	public final static int TYPE_STAFF = 1;
	public final static int TYPE_SWORD = 2;
	public final static int TYPE_HAMMER = 3;
	public final static int TYPE_DAGGER = 4;
	public final static int TYPE_BOW = 5;
	public final static int TYPE_CROSSBOW = 6;

	public Weapon(Player p, int t) {
		super(p);
		type = t;
	}

	public int weaponType(){return this.type;}
	public void setWeaponType(int t){this.type = t;}
	public static int typeForClass(int c){
		switch (c) {
		case 1:
			return TYPE_STAFF;
		case 2:
			return TYPE_SWORD;
		case 4:
			return TYPE_DAGGER;
		case 5:
			return TYPE_BOW;
		default:
			return 0;
		}
	}
}
