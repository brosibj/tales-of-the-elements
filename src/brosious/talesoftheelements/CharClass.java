package brosious.talesoftheelements;

import brosious.talesoftheelements.item.Armor;
import brosious.talesoftheelements.item.Weapon;

public class CharClass {

	private int c;
	private int armorLevel;
	private int weaponType;
	public final static int CLASS_WIZARD = 0;
	public final static int CLASS_ROGUE = 1;
	public final static int CLASS_ARCHER = 2;
	public final static int CLASS_KNIGHT = 3;
	
	public final static String[] CLASSES = {"Wizard","Rogue","Archer", "Knight"};
	
	public CharClass(int c) {
		this.c = c;
		armorLevel = Armor.typeForClass(c);
		weaponType = Weapon.typeForClass(c);
	}

	public int armorLevel(){return this.armorLevel;}
	public int weaponType(){return this.weaponType;}
	public int get(){return c;}
	public String getReadable(){return CLASSES[c];}
	public boolean isRanged(){
		if (c == CLASS_WIZARD || c == CLASS_ARCHER) return true;
		else return false;
	}
	public boolean canMelee(){
		if (c == CLASS_KNIGHT || c == CLASS_ROGUE) return true;
		else return false;
	}
	
	public void attack(Entity ent){
		//TODO: Script attacking another entity.
	}
}
