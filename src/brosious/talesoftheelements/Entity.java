package brosious.talesoftheelements;

import java.util.ArrayList;

import brosious.talesoftheelements.effect.*;

public class Entity {

	private String name;
	private int health;
	private int maxHealth;
	private int locX;
	private int locY;
	private boolean canDamage;
	private ArrayList<Effect> effectList = new ArrayList<Effect>();
	
	public Entity(String n) {
		name = n;
	}

	public Entity(String n, int x, int y){
		name = n;
		setX(x);
		setY(y);
	}
	
	
	/**
	 * @return the canDamage
	 */
	public boolean damagable() {
		return canDamage;
	}

	/**
	 * @param canDamage the canDamage to set
	 */
	public void setDamagable(boolean canDamage) {
		this.canDamage = canDamage;
	}

	public void setName(String n){name = n;}
	/**
	 * @return the locX
	 */
	public int x() {
		return locX;
	}

	/**
	 * @param locX the locX to set
	 */
	public void setX(int locX) {
		this.locX = locX;
	}

	/**
	 * @return the locY
	 */
	public int y() {
		return locY;
	}

	/**
	 * @param locY the locY to set
	 */
	public void setY(int locY) {
		this.locY = locY;
	}

	public String name(){return name;}
	public int maxHP(){return this.maxHealth;}
	public int hp(){return this.health;}
	public void damage(int d){health = health - d;}
	public void heal(int h){health = health + h;}
	public void addEffect(Effect e){effectList.add(e);}
	public void removeEffect(Effect e){effectList.remove(e);}
	public void removeIDEffect(int id){effectList.remove(id);}
	public ArrayList<Effect> effects(){return effectList;}
	public void tick(){
		if (!effectList.isEmpty()){
			for(int i=0; i<effectList.size(); i++){
				effectList.get(i).tick(this);
				if (effectList.get(i).length() <= 0){removeIDEffect(i);}
			}
		}
	}
	
}
