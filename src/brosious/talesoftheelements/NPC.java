package brosious.talesoftheelements;

public class NPC extends Entity {

	boolean isHostile = false;
	boolean isEnemy = false;
	int hostileRange;
	int tier;
	private int level;
	private CharClass c;
	private Element e;
	
	public final static int RARITY_COMMON = 0;
	public final static int RARITY_UNCOMMON = 1;
	public final static int RARITY_RARE = 2;
	public final static int RARITY_LEGENDARY = 3;
	public final static int RARITY_EPIC = 4;
	public final static String[] RARITIES = {"Common","Uncommon","Rare","Legendary","Epic"};

	public final static int MOB_NORMAL = 0;
	public final static int MOB_ELITE = 1;
	public final static int MOB_BOSS = 2;
	public final static int[][] CHANCE_RARITY = {
		{250, 600, 100, 035, 015},
		{100, 300, 350, 200, 050},
		{000, 150, 200, 400, 150}	};
	
	public NPC(String n) {
		super(n);
	}
	
	public NPC(String n, int x, int y){
		super(n,x,y);
	}
	
	public boolean isHostile(){return isHostile;}
	public boolean isEnemy(){return isEnemy;}
	public int aggroRange(){return hostileRange;}
	public void setTeir(int tier){this.tier = tier;}
	public int tier(){return this.tier;}
		
	public void setClass(CharClass c){this.c = c;}
	public void setElement(Element e){this.e = e;}
	public void setLevel(int l){this.level = l;}
	public int level(){return this.level;}
	public void addLevel(){this.level++;}
	public CharClass eClass(){return this.c;}
	public Element eElement(){return this.e;}

}
