package brosious.talesoftheelements.effect;

public class Burning extends Effect {

	public Burning(){
		super("Burning", Effect.EFFECT_DEBUFF);
		this.setStackLimit(5);
		this.setStacks(1);
		this.setLength(15);
	}
	
	public Burning(String name, int t) {
		super(name, t);
		// TODO Auto-generated constructor stub
	}

	public void setHitDamage(int d){this.setDPT((int) (d*0.15));}
}
