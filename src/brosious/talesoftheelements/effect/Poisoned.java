package brosious.talesoftheelements.effect;

public class Poisoned extends Effect{

	public Poisoned(){
		super("Burning", Effect.EFFECT_DEBUFF);
		this.setStackLimit(3);
		this.setStacks(1);
		this.setLength(20);
	}
	
	public Poisoned(String name, int t) {
		super(name, t);
		// TODO Auto-generated constructor stub
	}

	public void setHitDamage(int d){this.setDPT((int) (d*0.25));}
}
