package brosious.talesoftheelements.item;

import brosious.talesoftheelements.Player;

public class Armor extends Item {
	private int type;

	public final static int TYPE_JEWELRY = 0;
	public final static int TYPE_CLOTH = 1;
	public final static int TYPE_LEATHER = 2;
	public final static int TYPE_PLATE = 3;

	public final static int SLOT_HEAD = 0;
	public final static int SLOT_CHEST = 1;
	public final static int SLOT_LEGS = 2;
	public final static int SLOT_FEET = 3;
	public final static int SLOT_EARS = 4;
	public final static int SLOT_NECK = 5;
	public final static int SLOT_FINGER1 = 6;
	public final static int SLOT_FINGER2 = 7;
	
	
	public Armor(Player p, int t) {
		super(p);
		type = t;
	}

	public int armorType(){return this.type;}
	public void setArmorType(int t){this.type = t;}
	public static int typeForClass(int c){
		switch (c) {
		case 0:
			return TYPE_CLOTH;
		case 1:
			return TYPE_LEATHER;
		case 2:
			return TYPE_LEATHER;
		case 3:
			return TYPE_PLATE;
		default:
			return TYPE_CLOTH;
		}
	}
}
