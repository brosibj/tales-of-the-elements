package brosious.talesoftheelements;

import java.util.ArrayList;

import brosious.talesoftheelements.item.Armor;
import brosious.talesoftheelements.item.Item;
import brosious.talesoftheelements.item.Weapon;

public class Player extends NPC{
	
	private long exp;
	private ArrayList<Item> inventory = new ArrayList<Item>();
	
	
	public Player(String name, int l, CharClass c, Element e, long xp) {
		super(name);
		this.setLevel(l);
		this.setClass(c);
		this.setElement(e);
		exp = xp;
	}

	public void addItem(Item i){inventory.add(i);}
	public ArrayList<Item> inventory(){return inventory;}
	public void removeItem(Item i){inventory.remove(i);}
	public void setXP(long xp){exp = xp;}
	public void addXP(long xp){
		exp = exp + xp;
		if (xpUntilLevel() <= 0) levelUp();
	}
	public long xp(){return this.exp;}
	public long xpUntilLevel(){return exp;} //TODO: Some math to determine xp remaining until level.
	public long totalXPRequired(int l){return exp;} //TODO: Some math to determine total xp for a level.
	public void levelUp(){
		this.addLevel();
		//TODO: more things when leveled.
	}

	private boolean itemTests(Object o){
		if (((Item) o).requiredLevel() > this.level()) return false;
		if (o instanceof Armor && ((Armor)o).armorType() > this.eClass().armorLevel()) return false;
		if (o instanceof Weapon && ((Weapon)o).weaponType() > this.eClass().weaponType()) return false;
		return true;
	}
	public boolean canUse(Armor a){
		return itemTests(a);
	}
	
	public boolean canUse(Weapon w){
		return itemTests(w);
	}
	
	public boolean canUse(Item i){
		return itemTests(i);
	}
}
