package brosious.talesoftheelements;

public class Element {

	private int e;
	public final static int ELEMENT_NONE = 0;
	public final static int ELEMENT_FIRE = 1;
	public final static int ELEMENT_WATER = 2;
	public final static int ELEMENT_AIR = 3;
	public final static int ELEMENT_EARTH = 4;
	public final static int ELEMENT_LIGHT = 5;
	public final static int ELEMENT_DARK = 6;
	
	private final double CRIT_MOD = 1.15;
	
	public final String[] ELEMENTS = {"Fire", "Water", "Air", "Earth", "Light", "Dark"};
	private final double[] BONUS_FIRE	= {1, (1),  0.50, 1.15, 0.75, 1.25, 1.00};
	private final double[] BONUS_WATER	= {1, 1.25, (1),  1.00, 1.15, 0.75, 0.50};
	private final double[] BONUS_AIR	= {1, 0.75, 1.15, (1),  1.00, 0.50, 0.75};
	private final double[] BONUS_EARTH	= {1, 1.15, 0.75, 1.25, (1),  1.00, 0.50};
	private final double[] BONUS_LIGHT	= {1, 1.00, 1.15, 0.75, 0.50, (1),  1.25};
	private final double[] BONUS_DARK	= {1, 0.75, 1.00, 0.50, 1.25, 1.15, (1)};
	
	public Element(int element) {
		e = element;
	}
	
	public double critMod(NPC ent){
		if (e == ELEMENT_FIRE && ent.eElement().get() == ELEMENT_AIR) return CRIT_MOD;
		if (e == ELEMENT_WATER && ent.eElement().get() == ELEMENT_EARTH) return CRIT_MOD;
		if (e == ELEMENT_AIR && ent.eElement().get() == ELEMENT_WATER) return CRIT_MOD;
		if (e == ELEMENT_EARTH && ent.eElement().get() == ELEMENT_FIRE) return CRIT_MOD;
		if (e == ELEMENT_LIGHT && ent.eElement().get() == ELEMENT_WATER) return CRIT_MOD;
		if (e == ELEMENT_DARK && ent.eElement().get() == ELEMENT_LIGHT) return CRIT_MOD;
		return 1;
	}
	
	public double damageMod(NPC ent){
		switch (e) {
		case 0:
			return 0.5;
		case 1:
			return BONUS_FIRE[ent.eElement().get()];
		case 2:
			return BONUS_WATER[ent.eElement().get()];
		case 3:
			return BONUS_AIR[ent.eElement().get()];
		case 4:
			return BONUS_EARTH[ent.eElement().get()];
		case 5:
			return BONUS_LIGHT[ent.eElement().get()];
		case 6:
			return BONUS_DARK[ent.eElement().get()];
		default:
			return 0.5;
		}
		
	}
	
	public boolean appliesDOT(NPC ent){
		if (e == ELEMENT_FIRE){
			if (ent.eElement().get() == ELEMENT_EARTH || 
					ent.eElement().get() == ELEMENT_LIGHT || 
					ent.eElement().get() == ELEMENT_DARK){
				return true;
			}
		}
		else if(e == ELEMENT_DARK){
			if (ent.eElement().get() == ELEMENT_WATER || 
					ent.eElement().get() == ELEMENT_LIGHT || 
					ent.eElement().get() == ELEMENT_EARTH){
				return true;
			}
		}
		return false;
	}
	
	public int get(){return e;}
	public String getReadable(){return ELEMENTS[e];}
}
