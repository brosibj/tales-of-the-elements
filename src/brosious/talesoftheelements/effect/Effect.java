package brosious.talesoftheelements.effect;

import brosious.talesoftheelements.Entity;

public class Effect {
	
	private String n;
	private int stackLimit = 1;
	private int stacks = 1;
	private int damagePerTick = 0;
	private int length;
	private int type;
	
	public final static int EFFECT_BUFF = 0;
	public final static int EFFECT_DEBUFF = 1;

	public Effect(String name, int t) {
		n = name;
		this.type = t;
	}

	public void setStackLimit(int s){this.stackLimit = s;}
	public int stackLimit(){return this.stackLimit;}
	public void setStacks(int s){this.stacks = s;}
	public int stacks(){return this.stacks;}
	public void setDPT(int dpt){this.damagePerTick = dpt;}
	public int dpt(){return this.damagePerTick;}
	public void setLength(int l){this.length = l;}
	public int length(){return this.length;}
	public String name(){return this.n;}
	public int type(){return this.type;}
	
	public void tick(Entity ent){
		length--;
	}
}
